﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace UWPContactApp
{
    public sealed partial class MainPage : Page, INotifyPropertyChanged
    {
        public Contact CurrentContact;
        private ObservableCollection<Contact> contacts;
        public ObservableCollection<Contact> Contacts
        {
            get { return contacts; }
            set
            {
                if (value != contacts)
                {
                    contacts = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public MainPage()
        {
            this.InitializeComponent();

            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(440, 100));
            FillForm();
            IconsListBox.SelectedIndex = 0;
            CurrentContact = new Contact();
            Contacts = new ObservableCollection<Contact>();
            Contacts = LoadAllTheData();
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
        }

        private void IconsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBoxItem selectedItem = (ListBoxItem)IconsListBox.SelectedItem;

            switch (selectedItem.Name)
            {
                case "AddListBoxItem":
                    ExecuteButton.Content = "Add";
                        break;
                case "DeleteListBoxItem":
                    ExecuteButton.Content = "Delete";
                    break;
                case "UpdateListBoxItem":
                    ExecuteButton.Content = "Update";
                    break;
                default:
                    break;
            }
        }

        private void ExecuteButton_Click(object sender, RoutedEventArgs e)
        {
            switch ((string)ExecuteButton.Content)
            {
                case "Add":
                    Contact addContact = new Contact(-1, txtBxFirstName.Text, txtBxLastName.Text, DateTime.Parse(txtBxtxtBxDateOfBirth.Text), int.Parse(txtBxStreetNumber.Text), txtBxStreetName.Text, txtBxPostCode.Text, txtBxCity.Text, txtBxPhone1.Text, txtBxPhone2.Text, txtBxEmail.Text);
                    MySqlWorker.AddContact(addContact);
                    Contacts = LoadAllTheData();
                    ShowMessageBox("Contact added successfully");
                    break;
                case "Delete":
                    MySqlWorker.DeleteContact(CurrentContact.Id);
                    Contacts = LoadAllTheData();
                    ShowMessageBox("Contact deleted successfully");
                    break;
                case "Update":
                    Contact updateContact = new Contact(CurrentContact.Id, txtBxFirstName.Text, txtBxLastName.Text, DateTime.Parse(txtBxtxtBxDateOfBirth.Text), int.Parse(txtBxStreetNumber.Text), txtBxStreetName.Text, txtBxPostCode.Text, txtBxCity.Text, txtBxPhone1.Text, txtBxPhone2.Text, txtBxEmail.Text);
                    MySqlWorker.UpdateContact(updateContact);
                    Contacts = LoadAllTheData();
                    ShowMessageBox("Contact updated successfully");
                    break;
                default:
                    break;
            }
        }

        private void ContactsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((Contact)ContactsList.SelectedItem == null)
                return;

            CurrentContact = new Contact((Contact)ContactsList.SelectedItem);

            txtBxFirstName.Text = CurrentContact.FirstName;
            txtBxLastName.Text = CurrentContact.LastName;
            txtBxtxtBxDateOfBirth.Text = CurrentContact.DateOfBirth.ToString();

            txtBxStreetNumber.Text = CurrentContact.StreetNumber.ToString();
            txtBxStreetName.Text = CurrentContact.StreetName;
            txtBxPostCode.Text = CurrentContact.PostCode;
            txtBxCity.Text = CurrentContact.City;

            txtBxPhone1.Text = CurrentContact.Phone1;
            txtBxPhone2.Text = CurrentContact.Phone2;
            txtBxEmail.Text = CurrentContact.Email;
        }

        static ObservableCollection<Contact> LoadAllTheData()
        {
            var command = $"Select * from tbl_people";
            ObservableCollection<Contact> list = MySqlWorker.ShowInList(command);
            return list;
        }

        void FillForm()
        {
            txtBxFirstName.Text = "Michael";
            txtBxLastName.Text = "Cooper";
            txtBxtxtBxDateOfBirth.Text = "3/2/1993 12:00:00 AM";

            txtBxStreetNumber.Text = "360";
            txtBxStreetName.Text = "Duncan Ave";
            txtBxPostCode.Text = "4253";
            txtBxCity.Text = "New York";

            txtBxPhone1.Text = "056459326";
            txtBxPhone2.Text = "065128439";
            txtBxEmail.Text = "michaelcooper@gmail.com";
        }

        static async void ShowMessageBox(string message)
        {
            var dialog = new MessageDialog("");
            dialog.Title = message;

            dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            var res = await dialog.ShowAsync();

            if ((int)res.Id == 0)
            { }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }


}
